const Promise = require("bluebird");
const util = require("h13a-util");
const readme = require("../../../../README.md");
const packageJson = require("../../../../package.json");

// PLUGIN BLOCK about to intect a jq-plugin.

class Operation {

  constructor({ cardTask }) {
    this.cardTask = cardTask;
  }

  connection( { resolve, reject, element, block } ){

    let {
      name, // user specified plugin name
      selector, // user may state that the plugin is to be installed at multiple points
      options, // options opbject directly attached to plugin request
    } = block;

    let pluginName = name.toLowerCase().replace(/-/g,'');

    let el = null;

    if(! selector ){

      // create new DOM node
      el = $(`<div></div>`);
      element.append( el );

    }else{

      // select existing nodes
      el = $( selector, element );

    }

    if( $ ( el ) [ pluginName ] ) {

      $ ( el ) [ pluginName ] ( options );

    } else {

      console.log('plugin not available: ', pluginName);

    }

    resolve();
 
  }
}
module.exports = Operation;
