const Promise = require("bluebird");
const Operation = require("../operation/index.es6");

// You are in CARD / PLUGIN-BLOCK / <<jq-plugins follow>>

class Interface {

  constructor({ task, block }) {
    this.cardTask = task;
    this.blockMetadata = block;
  }

  creator() {

    let cardBlockPromise = new Promise((resolve, reject) => {

      // this is the card widget
      let card = this.cardTask.widget;

      // create a wrapper for all subnodes
      this.pluginNode = $(`<div class="plugin-block-container card-component"></div>`);
      card.element.append(this.pluginNode);
      let element = this.pluginNode;

      // extract from .this
      let { cardTask, blockMetadata } = this;

      // this is the operation class
      let operation = new Operation({ cardTask });

      // send parameters to operation class "connect to it"
      operation.connection({

        // Promise
        resolve,
        reject,

        // Helpful Information Isolated for the specific task at hand
        element, // print to this
        block: blockMetadata, // use information from here

      });

    }); // end creation of cardBlockPromise

    return cardBlockPromise;
  }


  destructor() {

    // executed by card when card being destroyed.
    // the card iterates over blocks it created,
    // this is a block that creates jq-plugins
    // therefore this block will have its destructor executed.

    // becasue jQuery Widgets have own destructors that are triggered automatically,
    // just delete all nodes.
    this.pluginNode.empty();

  }

}

module.exports = Interface;
