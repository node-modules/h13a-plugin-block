const Interface = require("./lib/interface/index.es6");

// Plugin Loader Plugins -- loading causes jquery plugin functions ($.fn.*) to be created

// BASE
require('h13a-jq-html');
require('h13a-jq-terminal');

require('h13a-jq-messages');



module.exports = Interface;
